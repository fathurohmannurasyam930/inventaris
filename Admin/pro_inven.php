<?php 
include 'database/class.php';
$db = new database();

/* Inventaris */
$aksi = $_GET['aksi'];
 if($aksi == "tambah"){
 	$db->input_inventaris($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['jenis'],$_POST['kondisi'],$_POST['asal_barang'],$_POST['ruang'],$_POST['keterangan'],$_POST['pegawai']);
 	header("location:inventarisir.php");
 }elseif($aksi == "hapus"){ 	
 	$db->hapus_inven($_GET['kode_inventaris']);
	header("location:inventarisir.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['asal_barang'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }

 /* Pinjaman */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah"){
 	$db->input_inventaris($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['jenis'],$_POST['kondisi'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:inventarisir.php");
 }elseif($aksi == "hapus_pinjam"){ 	
 	$db->hapus_pinjam($_GET['kode_peminjaman']);
	header("location:peminjaman.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }

  /* Pengembalian */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah"){
 	$db->input_inventaris($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['jenis'],$_POST['kondisi'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:inventarisir.php");
 }elseif($aksi == "hapus"){ 	
 	$db->hapus_inven($_GET['id_inventaris']);
	header("location:inventarisir.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }

  /* Ruang */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah_ruang"){
 	$db->input_ruang($_POST['kode_ruang'],$_POST['nama_ruang'],$_POST['keterangan']);
 	header("location:ruang.php");
 }elseif($aksi == "hapus_ruang"){ 	
 	$db->hapus_ruang($_GET['kode_ruang']);
	header("location:ruang.php");
 }elseif($aksi == "update_ruang"){
 	$db->update_ruang($_POST['id_ruang'],$_POST['kode_ruang'],$_POST['nama_ruang'],$_POST['keterangan']);
 	header("location:ruang.php");
 }

  /* pegawai */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah_pg"){
 	$db->input_pegawai($_POST['kode_pegawai'],$_POST['nip'],$_POST['nama_pegawai'],$_POST['username'],$_POST['password']);
 	header("location:pengguna.php");
 }elseif($aksi == "hapus_pg"){ 	
 	$db->hapus_pg($_GET['kode_pegawai']);
	header("location:pengguna.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }
  /* petugas */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah_pt"){
 	$db->input_petugas($_POST['kode_petugas'],$_POST['nama_petugas'],$_POST['username'],$_POST['password'],$_POST['level']);
 	header("location:pengguna.php");
 }elseif($aksi == "hapus_pt"){ 	
 	$db->hapus_pt($_GET['kode_petugas']);
	header("location:pengguna.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }

  /* asal barang */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah_ab"){
 	$db->input_ab($_POST['kode_asalbarang'],$_POST['asal_barang']);
 	header("location:asal_barang.php");
 }elseif($aksi == "hapus_ab"){ 	
 	$db->hapus_ab($_GET['kode_asalbarang']);
	header("location:asal_barang.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }

  /* Jenis */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah_jenis"){
 	$db->input_jenis($_POST['kode_jenis'],$_POST['nama_jenis'],$_POST['keterangan']);
 	header("location:jenis.php");
 }elseif($aksi == "hapus_jenis"){ 	
 	$db->hapus_jenis($_GET['kode_jenis']);
	header("location:jenis.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }

   /* Detail Pinjam */
 $aksi = $_GET['aksi'];
 if($aksi == "tambah"){
 	$db->input_inventaris($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['jenis'],$_POST['kondisi'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:inventarisir.php");
 }elseif($aksi == "hapus_dp"){ 	
 	$db->hapus_dp($_GET['id']);
	header("location:dp.php");
 }elseif($aksi == "update"){
 	$db->update_inven($_POST['kode_inventaris'],$_POST['nama_barang'],$_POST['jumlah'],$_POST['kondisi'],$_POST['jenis'],$_POST['jurusan'],$_POST['ruang'],$_POST['keterangan'],$_POST['petugas']);
 	header("location:tampil.php");
 }
?>