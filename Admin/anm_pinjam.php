<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>

				<header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Animasi Form Pinjam</h3>

                        </div>
                    </div>
                </div>
                </header>

                <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Peminjam
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Detail Peminjam</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                        <input type="text" class="form-control" readonly placeholder="KDP01">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Peminjaman</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                        <input type="text" class="form-control" readonly placeholder="KP01">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Petugas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                       <select name="jenis" id="select" class="form-control" required>
                                                         <?php  
                                                        foreach ($db->pegawai() as $pegawai) {
                                                        ?>
                                                <option value="<?php echo $pegawai['nama_pegawai']; ?>" ><?php echo $pegawai['nama_pegawai']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col col-md-1">
                                                        <label for="select" class=" form-control-label">Kelas</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                         <select name="kelas" id="select" type="text" class="form-control">
                                                            <option>Kelas</option>
                                                            <option>10 ANM 1</option>
                                                            <option>10 ANM 2</option>
                                                            <option>11 ANM 1</option>
                                                            <option>11 ANM 2</option>
                                                            <option>12 ANM 1</option>
                                                            <option>12 ANM 2</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                         <input type="text" name="kode_inventaris" class="form-control" placeholder="Kode Barang">
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" class="form-control" readonly placeholder="Nama Barang">
                                                    </div>
                                                    <div class="col col-md-1">
                                                        <label for="select" class=" form-control-label">Jenis</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" class="form-control" readonly placeholder="Jenis">
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kondisi</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" class="form-control" readonly placeholder="Kondisi">
                                                    </div>
                                                    <div class="col col-md-1">
                                                        <label for="select" class=" form-control-label">Ruang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" class="form-control" readonly placeholder="Ruang">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                                <button type="reset" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-ban"></i> Reset
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                    </div> 
                </div>


	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>