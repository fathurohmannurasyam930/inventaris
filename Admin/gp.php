<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Generate laporan</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                         <div class="row">
                                            <div class="col-md-6">
                                                Data Laporan Detail Pinjam
                                            </div>

                                            
                                       
                                         
                                        </div>
                                     
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>Kode Detail Peminjam</th>
                                                                <th>Kode Inventaris</th>
                                                                <th>Jumlah</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                foreach($db->detail_pinjam() as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $x['kode_detail_pinjam'];?></td>
                                                                    <td><?php echo $x['kode_inventaris'];?></td>
                                                                    <td><?php echo $x['jumlah']; ?></td>
                                                                    <td>
                                                                        <!--
                                                                        <a href="edit_barang.php">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fas fa-eye"></i> Edit
                                                                        </button>
                                                                        </a>
                                                                        -->
                                                                        <a href="pro_inven.php?kode_inventaris=<?php echo $x['kode_inventaris']; ?>&aksi=hapus">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>           </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                     <a href=".php">
                                            <button class="btn btn-danger btn-sm">
                                                <i class=""></i>PDF</button>
                                                </a>
                                         <a href=".php">
                                        <button class="btn btn-primary btn-sm">
                                            <i class=""></i>EXSEL</button>
                                        </a>
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>

<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>