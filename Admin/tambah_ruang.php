<?php
include"header.php";

include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Tambah Jenis Barang</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <!--
                    <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <button class="au-btn au-btn-icon au-btn--blue"><i class="fas fa-eye"></i>ANM</button>
                                    <button class="au-btn au-btn-icon au-btn--blue"><i class="fas fa-eye"></i>BC</button>
                                    <button class="au-btn au-btn-icon au-btn--blue"><i class="fas fa-eye"></i>RPL</button>
                                    <button class="au-btn au-btn-icon au-btn--blue"><i class="fas fa-eye"></i>TKR</button>
                                    <button class="au-btn au-btn-icon au-btn--blue"><i class="fas fa-eye"></i>TPL</button>
                                </div>
                            </div>
                        </div>
                         <hr>
                -->

               

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Tambah Ruang
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="pro_inven.php?aksi=tambah_ruang" method="post" class="form-horizontal">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Ruang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="kode_ruang" placeholder="Kode Ruang" class="form-control" >
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Ruang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_ruang" placeholder="Nama Ruang" class="form-control" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="Keterangan" placeholder="Keterangan" class="form-control" required>
                                                    </div>
                                                </div>

                                                

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Simpan">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
