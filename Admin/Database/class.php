<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "db_ujikom_fathur";
	var $koneksi;
	function __construct(){
		$this->koneksi = mysqli_connect($this->host, $this->uname, $this->pass, $this->db);
	}

	 /* INVENTARIS */
	function tampil_data_inven(){
	$data = mysqli_query($this->koneksi,"select * from inventaris");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}

	function input_inventaris($kode_inventaris,$nama_barang,$jumlah,$jenis,$kondisi,$asal_barang,$ruang,$keterangan,$pegawai){
		mysqli_query($this->koneksi,"insert into inventaris (kode_inventaris,nama_barang,jumlah,jenis,kondisi,asal_barang,ruang,keterangan,pegawai) values('$kode_inventaris','$nama_barang','$jumlah','$jenis','$kondisi','$asal_barang','$ruang','$keterangan','$pegawai')");
		}

	function hapus_inven($kode_inventaris){
		mysqli_query($this->koneksi,"delete from inventaris where kode_inventaris='$kode_inventaris'");
	}

	function edit($kode_inventaris){
		$data = mysqli_query($this->koneksi,"select * from inventaris where kode_inventaris='$kode_inventaris'");
		while($d = mysql_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
 
	function update_inven($kode_inventaris,$nama_barang,$jumlah,$jenis,$kondisi,$jurusan,$ruang,$keterangan,$petugas){
		mysqli_query($this->koneksi,"update inventaris set nama='$nama', alamat='$alamat', usia='$usia' where id='$id'");
	}

	/* Detail Pinjam */
	function detail_pinjam(){
	$data = mysqli_query($this->koneksi,"select * from detail_pinjam");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}


	function hapus_dp($id){
		mysqli_query($this->koneksi,"delete from detail_pinjam where id='$id'");
	}


	 /* Peminjaman */
	function peminjam(){
	$data = mysqli_query($this->koneksi,"select * from peminjaman");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}
	function hapus_pinjam($kode_peminjaman){
		mysqli_query($this->koneksi,"delete from peminjaman where kode_peminjaman='$kode_peminjaman'");
	}

	
	 /* Jenis */
	function jenis(){
	$data = mysqli_query($this->koneksi,"select * from jenis");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}
	function input_jenis($kode_jenis,$nama_jenis,$keterangan){
		mysqli_query($this->koneksi,"insert into jenis (kode_jenis,nama_jenis,keterangan) values('$kode_jenis','$nama_jenis','$keterangan')");
		}

	function hapus_jenis($kode_jenis){
		mysqli_query($this->koneksi,"delete from jenis where kode_jenis='$kode_jenis'");
	}

	 /* Asal barang */
	function asal_barang(){
	$data = mysqli_query($this->koneksi,"select * from asal_barang");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}
	function hapus_ab($kode_asalbarang){
		mysqli_query($this->koneksi,"delete from asal_barang where kode_asalbarang='$kode_asalbarang'");
	}
	function input_ab($kode_asalbarang,$asal_barang){
		mysqli_query($this->koneksi,"insert into asal_barang (kode_asalbarang,asal_barang) values('$kode_asalbarang','$asal_barang')");
		}

	 /* Ruang */
	function ruang(){
	$data = mysqli_query($this->koneksi,"select * from ruang");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}
	function hapus_ruang($kode_ruang){
		mysqli_query($this->koneksi,"delete from ruang where kode_ruang='$kode_ruang'");
	}
	function input_ruang($kode_ruang,$nama_ruang,$keterangan){
		mysqli_query($this->koneksi,"insert into ruang (kode_ruang,nama_ruang,keterangan) values('$kode_ruang','$nama_ruang','$keterangan')");
		}
	function edit_ruang($id_ruang){
		$data = mysqli_query($this->koneksi,"select * from ruang where id_ruang='$id_ruang' ");
		
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function update_ruang($id_ruang,$kode_ruang,$nama_ruang,$keterangan){
		mysqli_query($this->koneksi,"update ruang set kode_ruang='$kode_ruang', nama_ruang='$nama_ruang', keterangan='$keterangan' where id_ruang='$id_ruang' ");
	}

	 /* Petugas */
	function petugas(){
	$data = mysqli_query($this->koneksi,"select * from petugas");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}
	function hapus_pt($kode_petugas){
		mysqli_query($this->koneksi,"delete from petugas where kode_petugas='$kode_petugas'");
	}
	function input_petugas($kode_petugas,$nama_petugas,$username,$password,$level){
		mysqli_query($this->koneksi,"insert into petugas (kode_petugas,nama_petugas,username,password,level) values('$kode_petugas','$nama_petugas','$username','$password','$level')");
		}

	/* Pegawai */
	function pegawai(){
	$data = mysqli_query($this->koneksi,"select * from pegawai");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}
	function hapus_pg($kode_pegawai){
		mysqli_query($this->koneksi,"delete from pegawai where kode_pegawai='$kode_pegawai'");
	}
	function input_pegawai($kode_pegawai,$nip,$nama_pegawai,$username,$password){
		mysqli_query($this->koneksi,"insert into pegawai (kode_pegawai,nip,nama_pegawai,username,password) values('$kode_pegawai','$nip','$nama_pegawai','$username','$password')");
		}

	/* Level */
	function level(){
	$data = mysqli_query($this->koneksi,"select * from level");

		while($d = mysqli_fetch_array($data)){
		$hasil[] = $d;
		}
		return $hasil;
	}





	}
?>