<?php
include"header.php";

include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Tambah Barang</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">
               

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Tambah barang
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="pro_inven.php?aksi=tambah" method="post" class="form-horizontal">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Invetaris</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="kode_inventaris" placeholder="Kode Inventaris" class="form-control" >
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_barang" placeholder="Nama barang" class="form-control" required>
                                                    </div>
                                                    <div class="col col-md-1">
                                                        <label for="select" class=" form-control-label">Jumlah</label>
                                                    </div>
                                                    <div class="col-12 col-md-1">
                                                        <input type="number" name="jumlah" class="form-control" value="0" required>
                                                    </div>
                                                </div>

                                                 

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Jenis</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <select name="jenis" id="select" class="form-control" required>
                                                         <?php  
                                                        foreach ($db->jenis() as $jenis) {
                                                        ?>
                                                <option value="<?php echo $jenis['nama_jenis']; ?>" ><?php echo $jenis['nama_jenis']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kondisi</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <select name="kondisi" id="select" class="form-control" required>
                                                        <option>Baik</option>
                                                        <option>Kurang baik</option>
                                                        <option>Rusak</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Untuk Barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <select name="asal_barang" id="select" class="form-control" required>
                                                         <?php  
                                                        foreach ($db->asal_barang() as $ab) {
                                                        ?>
                                                <option value="<?php echo $ab['asal_barang']; ?>" ><?php echo $ab['asal_barang']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Ruangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <select name="ruang" id="select" class="form-control" required>
                                                        <?php  
                                                        foreach ($db->ruang() as $ruang) {
                                                        ?>
                                                <option value="<?php echo $ruang['nama_ruang']; ?>" ><?php echo $ruang['nama_ruang']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="keterangan" placeholder="Keteranagn" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <select name="pegawai" id="select" class="form-control" required>
                                                        <?php  
                                                        foreach ($db->pegawai() as $pegawai) {
                                                        ?>
                                                <option value="<?php echo $pegawai['kode_pegawai']; ?>" ><?php echo $pegawai['nama_pegawai']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Simpan">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
