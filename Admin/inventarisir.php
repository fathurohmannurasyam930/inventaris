<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Inventarisir</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                    <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Barang</h2>
                                    <a href="tambah_barang.php">
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>Tambah Barang</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                


                <hr>

                 <div class="row" align="center">
                    <div class="col-lg-4">
                                    <div class="au-card recent-report">
                                       <h2>ANM</h2>
                                       <span>Animasi</span>
                                       <hr>
                                       <a href="anm.php"><button class="btn btn-info" type="button">Lihat</button></a>
                                    </div>
                    </div> 
                    <div class="col-md-4">
                                     <div class="au-card recent-report">
                                       <h2>BC</h2>
                                        <span>Brocasting</span>
                                        <hr>
                                       <a href="bc.php"><button class="btn btn-info" type="button">Lihat</button></a>
                                    </div>
                    
                    </div>  
                    <div class="col-lg-4">
                                    <div class="au-card recent-report">
                                        <h2>RPL</h2>
                                        <span>Rekayasa Perangkat Lunak</span>
                                        <hr>
                                       <a href="rpl.php"><button class="btn btn-info" type="button">Lihat</button></a>
                                    </div>
                    </div> 
                </div>

                 <div class="row" align="center">
                   
                    <div class="col-md-6">
                                     <div class="au-card recent-report">
                                       <h2>TKR</h2>
                                       <span>Teknik Kendaran Ringan</span>
                                        <hr>
                                       <a href="tkr.php"><button class="btn btn-info" type="button">Lihat</button></a>
                                    </div>
                    
                    </div> 
                    <div class="col-lg-6">
                                    <div class="au-card recent-report">
                                        <h2>TPL</h2>
                                       <span>Teknik Pengelasan</span>
                                        <hr>
                                       <a href="tpl.php"><button class="btn btn-info" type="button">Lihat</button></a>
                                    </div>
                    </div>
                </div>
                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                        <div class="row">
                                            <div class="col-md-6">
                                                 Data Inventaris
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <a href=".php">
                                            <button class="btn btn-danger btn-sm">
                                                <i class=""></i>PDF</button>
                                                </a>
                                         <a href=".php">
                                        <button class="btn btn-primary btn-sm">
                                            <i class=""></i>EXSEL</button>
                                        </a>
                                            </div>

                                       
                                         
                                        </div>
                                        

                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>Kode Inventaris</th>
                                                                <th>Nama Barang & Jenis </th>
                                                                <th>Kondisi</th>
                                                                <th>Barang & Jumlah</th>
                                                                <th>Tanggal Register</th>
                                                                <th>Keterangan & Ruang</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                $no = 1 ;
                                                                foreach($db->tampil_data_inven() as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $x['kode_inventaris'];?></td>
                                                                    <td><?php echo $x['nama_barang'];?>
                                                                         / <?php echo $x['jenis']; ?>
                                                                    </td>
                                                                    <td><?php echo $x['kondisi']; ?></td>
                                                                    <td><?php echo $x['asal_barang']; ?>
                                                                         : <?php echo $x['jumlah']; ?>
                                                                    </td>
                                                                    <td><?php echo $x['tanggal_register']; ?></td>
                                                                    <td><?php echo $x['keterangan']; ?>
                                                                         : <?php echo $x['ruang']; ?>
                                                                    </td>
                                                                    <td>
                                                                       
                                                                        <a href="edit_barang.php?kode_inventaris=<?php echo $x['kode_inventaris'];?>">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fas fa-eye"></i> Edit
                                                                        </button>
                                                                        </a>
                                                                       
                                                                        <a href="pro_inven.php?kode_inventaris=<?php echo $x['kode_inventaris']; ?>&aksi=hapus">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>           
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                </div>
            </div>
            <hr>

	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>

<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>