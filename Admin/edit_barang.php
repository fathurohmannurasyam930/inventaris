<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Edit Barang</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">
               

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Edit barang
                                        </div>
                                        <div class="card-body card-block">
                                        <?php
                                                    include"database/koneksi.php";
                                                    $kode_inventaris=$_GET['kode_inventaris'];
                                                    $pilih=mysqli_query($koneksi, "SELECT * FROM inventaris WHERE kode_inventaris='$kode_inventaris'");
                                                    $tampil=mysqli_fetch_array($pilih);
                                            ?>
                                            <form action="" method="post" class="form-horizontal">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class="form-control-label">Kode Inventaris</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                    <input type="hidden" name="kode_inventaris" value="<?php echo $_GET['kode_inventaris'];?>">
                                                    <input type="text" name="kode_inventaris" class="form-control" value="<?php echo $tampil['kode_inventaris'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input value="<?php echo $tampil['nama_barang'];?>" type="text" name="nama_barang" class="form-control" required>
                                                    </div>
                                                    <div class="col col-md-1">
                                                        <label for="select" class=" form-control-label">Jumlah</label>
                                                    </div>
                                                    <div class="col-12 col-md-1">
                                                        <input value="<?php echo $tampil['jumlah'];?>" type="number" name="jumlah" class="form-control" value="0" required>
                                                    </div>
                                                </div>

                                                 

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Jenis</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input value="<?php echo $tampil['jenis'];?>" type="text" name="jenis" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kondisi</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input value="<?php echo $tampil['kondisi'];?>" type="text" name="kondisi" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Untuk Barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                       <input value="<?php echo $tampil['asal_barang'];?>" type="text" name="asal_barang" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Ruangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input value="<?php echo $tampil['ruang'];?>" type="text" name="ruang" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input value="<?php echo $tampil['keterangan'];?>" type="text" name="keterangan" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input value="<?php echo $tampil['pegawai'];?>" type="text" name="pegawai" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Edit" name="edit">
                                                </div>
                                            </form>
                                             <?php
                                            include"database/koneksi.php";
                                            if(isset($_POST['edit'])){
                                                $kode_inventaris=$_POST['kode_inventaris'];
                                                $nama_barang=$_POST['nama_barang'];
                                                $jumlah=$_POST['jumlah'];
                                                $jenis=$_POST['jenis'];
                                                $kondisi=$_POST['kondisi'];
                                                $asal_barang=$_POST['asal_barang'];
                                                $ruang=$_POST['ruang'];
                                                $keterangan=$_POST['keterangan'];
                                                $pegawai=$_POST['pegawai'];




                                                $input=mysqli_query($koneksi, "UPDATE inventaris SET kode_inventaris='$kode_inventaris', nama_barang='$nama_barang', jumlah='$jumlah', jenis='$jenis', kondisi='$kondisi', asal_barang='$asal_barang', ruang='$ruang', keterangan='$keterangan', pegawai='$pegawai' WHERE kode_inventaris='$kode_inventaris'");

                                                if ($input) {
                                                    echo "Berhasil";
                                                    ?>
                                                    <script type="text/javascript">
                                                        window.location.href="inventarisir.php";
                                                    </script>
                                                    <?php
                                                }else{
                                                    echo"gagal";
                                                }
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
